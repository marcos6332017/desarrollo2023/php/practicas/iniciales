<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

    <?php

    $radio = 2.4;
    $perimetro = 2 * M_PI * $radio;
    $area = pi() * $radio ** 2;
    ?>
    <div>
        El radio del ciculo es <?= $radio ?>
    </div>
    <div>
        El area del circulo es <?= $area ?>
    </div>
    <div>
        El perimetro del circulo es <?= $perimetro ?>
    </div>
</body>

</html>