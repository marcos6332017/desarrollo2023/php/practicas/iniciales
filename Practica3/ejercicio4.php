<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    $numeros = [
        "cero" => 0,
        "uno" => 1,
        "dos" => 2,
        "tres" => 3,
        "cuatro" => 4,

    ];
    $vocales = [
        "a", "e", "i", "o", "u"
    ];

    echo ("{$numeros["uno"]}<br>");

    echo ("{$vocales[1]}<br>");

    $vocales[] = "á";
    var_dump($vocales);

    $numeros["cinco"] = 5;
    var_dump($numeros);
    ?>
</body>

</html>