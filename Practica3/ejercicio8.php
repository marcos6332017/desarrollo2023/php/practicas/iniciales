<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    $datos = [

        [
            "nombre" => "Eva",
            "edad" => 50
        ],
        [
            "nombre" => "Jose",
            "edad" => 40,
            "peso" => 80
        ],
        [
            "nombre" => "Lorena",
            "edad" => 80,
            "altura" => 1.75
        ]
    ];

    array_push(
        $datos,
        [
            "nombre" => "Luis",
            "edad" => 20,
            "peso" => 90

        ],
        [
            "nombre" => "Oscar",
            "edad" => 23
        ]
    );
    echo "<pre>";
    var_dump($datos);
    echo "</pre>";
    ?>
</body>

</html>