<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    $a = 35;
    $b = ["poco", "algo", "medio", "mucho", "enorme"];

    if ($a < 10) {
        echo $b[0];
    } elseif ($a < 20) {
        echo $b[1];
    } elseif ($a < 30) {
        echo $b[2];
    } elseif ($a < 40) {
        echo $b[3];
    } else {
        echo $b[4];
    }
    ?>
</body>

</html>